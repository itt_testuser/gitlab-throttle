# Throttle Gitlab Pipelines v0.0.1

(c) Ian Norton 2019

This tool should allow a gitlab CI pipeline to have a maximum number
of running pipelines per branch.

The idea is that you put it in the first stage of your pipeline, 
it starts and uses the gitlab API to see if there are any other
pipelines of this project running on the same branch.

If there are, and are more than the max allowed number then it will
use the API to abort the oldest pipeline.

It might be helpful to exclude the throttle job from tags or refs like 
master

The tool must have a private API token set as a CI variable called `GITLAB_API_TOKEN` 
that must be allowed to list and cancel pipelines

The max limit is set to 2 running pipelines but can be changed by setting `GITLAB_MAX_BRANCH_PIPELINES` to a
positive integer